package me.xethh.libs.toolkits.akka.commons;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

/**
 * Base configuration for spring
 */
@Configuration
@ComponentScan("me.xethh.libs.toolkits.akka.commons")
public class AppSpringConfig
{
}
