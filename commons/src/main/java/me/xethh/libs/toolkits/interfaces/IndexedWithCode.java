package me.xethh.libs.toolkits.interfaces;

public interface IndexedWithCode {
    String code();
}
