package me.xethh.libs.toolkits.sql;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ResultSetIterable implements Iterable<ResultSet> {
    private ResultSet resultSet;
    public ResultSetIterable(ResultSet resultset){
        this.resultSet = resultset;
    }

    @Override
    public Iterator<ResultSet> iterator() {
        return new ResultSetIterator(resultSet);
    }

}
