package me.xethh.libs.toolkits.stopWatchEx;

public interface StartStopLabeling {
    String getLabel();
    String startLabel();
    String stopLabel();
}
