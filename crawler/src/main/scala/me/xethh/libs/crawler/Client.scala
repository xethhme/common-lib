package me.xethh.libs.crawler

import me.xethh.libs.crawler.cookies.{CookieStorage, MemoryCookiesStorage}
import org.apache.http.client.methods.{CloseableHttpResponse, HttpGet, HttpRequestBase}
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.{BasicCookieStore, CloseableHttpClient}
import org.apache.http.protocol.BasicHttpContext
import org.apache.http.util.EntityUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import scala.collection.mutable.ListBuffer

class Client(
              val client: CloseableHttpClient,
              cookieProvider:()=>CookieStorage[BasicCookieStore]=()=>new MemoryCookiesStorage(),
              contextProvider:()=>BasicHttpContext= ()=>new BasicHttpContext()
            ) {
  val cookieStore:CookieStorage[BasicCookieStore] = cookieProvider()
  val context = contextProvider.apply()

  def get(_url:String):GetRequest=new GetRequest(_url,this)
  def exec[R <: HttpRequestBase](r: R)={
    context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore.getBasicCookieStore())
    val response = client.execute(r, context)
    cookieStore.store(context.getAttribute(HttpClientContext.COOKIE_STORE).asInstanceOf[BasicCookieStore])
    response
  }

}
class GetRequest(_url:String,client:Client){
  val headers:ListBuffer[(String,String)] = ListBuffer.empty[(String,String)]
  var params:ListBuffer[(String,String)] = ListBuffer.empty[(String,String)]
  def setHeader(name:String, value:String):GetRequest = {
    headers += Tuple2(name,value)
    this
  }
  def setRefer(value:String):GetRequest=setHeader("Referer",value)
  def execute:CloseableHttpResponse={
    val request = new HttpGet(_url)
    headers.foreach(x=>request.setHeader(x._1,x._2))
    val uriBuilder = new URIBuilder(_url)
    params.foreach(x=>uriBuilder.setParameter(x._1,x._2))
    request.setURI(uriBuilder.build())
    client.exec(request)
  }
  def setParam(key:String,value:String):GetRequest={
    params += Tuple2(key,value)
    this
  }
  def executeAsString:String={
    EntityUtils.toString(execute.getEntity)
  }
  def executeAsJsoup(url:String):Document={
    Jsoup.parse(
      executeAsString,url
    )
  }
  def executeAsJsoup:Document={
    Jsoup.parse(
      executeAsString,_url
    )
  }
}
object Client{

  def main(args: Array[String]): Unit = {
  }

}

