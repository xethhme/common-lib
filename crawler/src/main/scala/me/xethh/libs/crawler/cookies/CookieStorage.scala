package me.xethh.libs.crawler.cookies

import org.apache.http.client.CookieStore

trait CookieStorage[C <: CookieStore] {
  def reset():C
  def getBasicCookieStore():C
  def store(cookie:C):C
  def modify(oper:C=>C):C = store(oper(getBasicCookieStore()))
}
