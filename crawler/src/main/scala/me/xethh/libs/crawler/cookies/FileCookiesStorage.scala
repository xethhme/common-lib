package me.xethh.libs.crawler.cookies

import java.io.{File, FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}
import java.util.Date

import org.apache.http.impl.client.BasicCookieStore

class FileCookiesStorage(filename:String, timeout:Long) extends CookieStorage[BasicCookieStore] {
  override def getBasicCookieStore(): BasicCookieStore = {
    val file = new File(filename)
    if(!file.exists())
      store(new BasicCookieStore)

    if((new Date).getTime - file.lastModified() > timeout) {
      file.delete()
      store(new BasicCookieStore)
    }

    val is = new ObjectInputStream(new FileInputStream(file))
    val obj = is.readObject().asInstanceOf[BasicCookieStore]
    is.close()
    obj
  }

  override def store(cookie: BasicCookieStore): BasicCookieStore = {
    val os = new ObjectOutputStream(new FileOutputStream(filename))
    os.writeObject(cookie)
    os.flush()
    os.close()
    getBasicCookieStore()
  }

  override def reset(): BasicCookieStore = {
    val file = new File(filename)
    if(file.exists())
      file.delete()
    getBasicCookieStore()
  }
}
