package me.xethh.libs.crawler.cookies

import org.apache.http.impl.client.BasicCookieStore

class MemoryCookiesStorage extends CookieStorage[BasicCookieStore] {
  var cookies = new BasicCookieStore()
  override def getBasicCookieStore(): BasicCookieStore = cookies

  override def store(cookie: BasicCookieStore): BasicCookieStore = {
    this.cookies=cookie
    cookie
  }

  override def reset(): BasicCookieStore = {
    cookies = new BasicCookieStore
    getBasicCookieStore()
  }
}
