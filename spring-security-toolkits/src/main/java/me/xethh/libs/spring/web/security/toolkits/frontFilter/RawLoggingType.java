package me.xethh.libs.spring.web.security.toolkits.frontFilter;

public enum RawLoggingType{
    Empty, Http, OtherServlet,
    FeignReq, FeignRes
}

