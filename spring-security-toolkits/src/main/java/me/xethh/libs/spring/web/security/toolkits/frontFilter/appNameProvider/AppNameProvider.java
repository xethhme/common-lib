package me.xethh.libs.spring.web.security.toolkits.frontFilter.appNameProvider;

@FunctionalInterface
public interface AppNameProvider {
    String gen();
}
