package me.xethh.libs.toolkits.webDto.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import me.xethh.libs.toolkits.threadLocal.XEContext;
import me.xethh.libs.toolkits.threadLocal.XEContextRaw;
import me.xethh.libs.toolkits.webDto.core.MetaEntity;

import java.util.Date;

public class WebBaseEntity {
    private String id;
    @JsonIgnore
    private MetaEntity meta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MetaEntity getMeta() {
        return meta;
    }

    public void setMeta(MetaEntity meta) {
        this.meta = meta;
    }
}
