package me.xethh.libs.toolkits.webDto.core.request.search;

import me.xethh.libs.toolkits.webDto.core.Chain.Chain;
import me.xethh.libs.toolkits.webDto.core.request.Request;
import me.xethh.libs.toolkits.webDto.core.request.RequestWithArgs;
import me.xethh.libs.toolkits.webDto.core.request.search.SearchingCriteria;

public class SearchingRequest<Self extends SearchingRequest<Self, Search>, Search extends SearchingCriteria> extends RequestWithArgs<Self, Search> {

    public Search getSearch() {
        return getData();
    }

    public void setSearch(Search data){
        search(data);
    }
    public Self search(Search data){
        return Chain.get((Self s)->s.setData(data)).apply((Self)this);
    }
}
